﻿using Calc.Core.ServiceInterfaces.Operators;
using System;

namespace Calc.Core.Domain
{
    public class UnaryCalcNode : CalcNode
    {
        public UnaryCalcNode(IUnaryOperator @operator, CalcNode argument)
        {
            Operator = @operator;
            Argument = argument;
        }

        public override decimal GetValue()
        {
            return Operator.Exec(Argument.GetValue());
        }

        public override string GetString()
        {
            return $"{Operator.Symbol}{Argument.GetString()}";
        }

        public IUnaryOperator Operator { get; }
        public CalcNode Argument { get; }
    }

}
