# README #



## Kuidas repoot kasutada? ##

* Tehke endale fork
* Oma repos, mis te forkisite, ärge masterisse ise midagi pushige, et vältida tulevikus merge konflikte

## 7. nädal (10.10.2018)

### Ülesanne

* Luua home lehele input väli, kuhu saab sisestada expressioni ja saata see api-le
* kuvada api-st saadud tulemus

## 6. nädal (3.10.2018)

* ++x vs x--
* x() && y() - kui esimene operatsioon on false, siis y() käima ei lasta
* Singleton pattern
* DI (dependency injectoni) lifestyled (singleton, scoped, transient)
* API routing
* HTTP verbs
* REST

### Ülesanne

* Avalides parsimine peab aru saama mitmetest alam-avaldisest samal tasemel. nt: 1 + (1+2) * (3+1)

## 5. nädal (26.09.2018)

* yield return
* generikud
* sulgude parsimine (lihtsustatud)

## 4. nädal (19.09.2018) ##

* yield return
* anonyymsed funktsioonid

### Ülesanne ###

* Avaldise stringi parsimine puu struktuuriks (pushige oma töö oma reposse ja saatke mulle link)

## 3. nädal (12.09.2018) ##

### Ülesanne ###

* Puust avaldise stringi koostamine

### kasulikud lingid ###

* [Visual studio keyboard shortcuts](https://docs.microsoft.com/en-us/visualstudio/ide/default-keyboard-shortcuts-in-visual-studio?view=vs-2017)