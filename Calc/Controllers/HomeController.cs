using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Calc.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }
        [HttpPost]
        public string Post([FromBody] PostModelData obj)
        {
            string myObj = "Success";
            return myObj.ToString();
        }
    }
}
public class PostModelData
{
    public string FstVarValue;
    public string SndVarValue;
}