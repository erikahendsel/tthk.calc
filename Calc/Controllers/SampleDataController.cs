using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Calc.ApplicationServices;
using Calc.Core.ApplicationServices;
using Microsoft.AspNetCore.Mvc;

namespace Calc.Controllers
{
    [Route("api/[controller]")]
    public class BaseApiController : Controller
    {

    }

    public class CalculatorController : BaseApiController
    {
        private readonly ICalculatorService _calculatorService;

        public CalculatorController(ICalculatorService calculatorService)
        {
            _calculatorService = calculatorService;
        }

        public IActionResult Calc(string expression)
        {
            var res = _calculatorService.EvalExpression(expression);
            return Ok(res);
        }
    }

    public class SampleDataController : BaseApiController
    {
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        public IActionResult Hello()
        {
            return Ok("Hello World!");
        }

        [HttpPost]
        public IActionResult Hello2()
        {
            return Ok("Hello World!");
        }

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
